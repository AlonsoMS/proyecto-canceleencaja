import SpinnerLoader from '../components/spinner.component'
import { useState } from 'react'
import BoletasList from '../components/boletasList.component'


function VentaPanel (props) {
    const [loaded, setLoadedState] = useState(false)
    
    return (
        <div>
            <SpinnerLoader dataLoaded={loaded}/>
            <BoletasList setLoadedState={setLoadedState} loaded={loaded}/>
        </div>
    )
}

export default VentaPanel