import { useState, useEffect} from 'react'
import axios from 'axios'
import { Container,Table } from 'react-bootstrap'
import { RiCheckboxCircleLine} from "react-icons/ri";

function ProductsList (props) {
    const [boletasData, setBoletasData] = useState([])
    const [boletasPagData, setBoletasPagData] = useState([])

    const pagarBoleta = (boleta) => {
        let tmp=[]
        let tmp2=[]
        for (var i=0; i<boletasPagData.length; i++){
            tmp.push(boletasPagData[i])
        }
        for (var u=0; u<boletasData.length; u++){
            if(u !== boletasData.indexOf(boleta))tmp2.push(boletasData[u])
        }
        tmp.push(boleta)
        setBoletasPagData(tmp)
        setBoletasData(tmp2)
    }
    useEffect(() => {
        const fetchData = async () => {
            if (!props.loaded) {
                const result = await axios.get('http://localhost:3001/verBoletas')
                if (result.data.status === "OK") { 
                    props.setLoadedState(true)
                    setBoletasData(result.data.boletas)
                }
            }
        }

        fetchData()
    })
    return(
        <div style={{
            backgroundColor: '#fce0e3',
          }}>
        <br/>
        <Container><h1 style={{color: '#dc3545'}}>Caja</h1></Container>
        <br/>
        <Container><h3>Boletas Emitidas</h3></Container>
        
        <Container >
        <Table striped bordered hover >
            <thead>
                <tr>
                <th>ID</th>
                <th>Fecha - Hora</th>
                <th>Monto</th>
                <th>Pagar</th>
                </tr>
            </thead>
            <tbody>
                {boletasData.map((boleta, index) => 
                    <tr key={boleta.id}>
                        <td>{boleta.id}</td>
                        <td>{boleta.fecha_hora}</td>
                        <td >${boleta.monto}</td>
                        <td ><RiCheckboxCircleLine onClick={() => pagarBoleta(boleta)} style={{height: 30, width: 30,color: '#007bff'}}/></td>
                    </tr>
                 )}
            </tbody>
        </Table>

        <Table striped bordered hover >
            <thead>
                <tr>
                <th>ID</th>
                <th>Fecha - Hora</th>
                <th>Monto</th>
                </tr>
            </thead>
            <tbody>
                {boletasPagData.map((boleta, index) => 
                    <tr key={boleta.id}>
                        <td>{boleta.id}</td>
                        <td>{boleta.fecha_hora}</td>
                        <td >${boleta.monto}</td>
                    </tr>
                 )}
            </tbody>
        </Table>
        </Container>
        <br/>
        <br/>
        <br/>
        <br/>
        </div>
        )
}

export default ProductsList
