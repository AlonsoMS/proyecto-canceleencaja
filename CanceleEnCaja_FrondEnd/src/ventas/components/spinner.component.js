import {Spinner } from 'react-bootstrap'

function SpinnerLoader (props) {
    if (props.dataLoaded) {
        return null
    } else {
        return (
            <div>
            <div style={{position:"fixed", top:"50%", left:"51%", transform:"translate(-50%, -50%)"}}>
                <Spinner animation="grow" variant="danger" />
            </div>
            <div style={{position:"fixed", top:"50%", left:"49%", transform:"translate(-50%, -50%)"}}>
                <Spinner animation="grow" variant="warning"  />
            </div>
            
            </div>
            
            
        )
    }
}

export default SpinnerLoader