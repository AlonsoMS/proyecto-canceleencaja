import { useState, useEffect} from 'react'
import axios from 'axios'
import { Container, Form,Table, FormControl,Button } from 'react-bootstrap'
import { RiAddCircleLine, RiCloseCircleLine } from "react-icons/ri";


function ProductsList (props) {
    const [productsData, setProductsData] = useState([])
    const [productsDataTmp, setProductsDataTmp] = useState([])
    const [boletaData, setBoletaData] = useState([])
    const [cant, setCant] = useState('')
    const [total, setTotal] = useState(0)
    const [btnState, setBtnState] = useState(true)
    
    const handleCantChange = (event) => {
        setCant(event.target.value)
    }
    const busqueda = (event) => {
        let tmp3=[]
        for (let i=0;i<productsDataTmp.length;i++){
            let codigoTmp=(productsDataTmp[i].codigoBarra).toString()
            let nameTmp=productsDataTmp[i].name
            console.log(nameTmp)
            if (nameTmp.includes(event.target.value.toUpperCase()) ||  codigoTmp.includes(event.target.value)){
                tmp3.push(productsDataTmp[i])
            }
        }
        setProductsData(tmp3)

    }

    const agregarBoleta = async() =>{
        let hoy = new Date();
        let fecha = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
        let hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
        const agregar = await axios.post(('http://localhost:3001/venta/agregarBoleta'),     
                                {
                                    "fecha_hora": fecha+"  "+hora,
                                    "monto": total
                                    } 
                  )
                  if(agregar.data.status === 'OK'){
                    setBoletaData([])
                    setTotal(0)
                  }
                  console.log(agregar.data) 
    }
    
    const addProductBoleta=(producto)=>{
        if(producto.cantidad<producto.cantidadV){
            alert('CANTIDAD EXCEDE EL MÁXIMO EN BODEGA')
        }
        else{
            if (producto.cantidadV && typeof(producto.cantidadV)==='number'){
                setBtnState(false)
                setTotal(total+(producto.cantidadV*producto.precio))
                let tmp2=[]
                for (var i=0; i<boletaData.length; i++){
                    tmp2.push(boletaData[i])
                }
                tmp2.push(producto)
                setBoletaData(tmp2)
            }
            else{
                alert('DEBE INGRESAR UNA CANTIDAD VÁLIDA')
            }
        }
        setCant('')

    }

    const deleteProductBoleta=(index)=>{
        let tmp=boletaData
        setTotal(total-(tmp[index].cantidadV*tmp[index].precio))
        tmp.splice(index, 1);
        if (tmp.length===0){
            setBtnState(true)
        }
        setBoletaData(tmp)
        

    }
    useEffect(() => {
        const fetchData = async () => {
            if (!props.loaded) {
                const result = await axios.get('http://localhost:3001/inventario')
                if (result.data.status === "OK") { 
                    props.setLoadedState(true)
                    setProductsData(result.data.productos)
                    setProductsDataTmp(result.data.productos)
                }
            }
        }

        fetchData()
    })
    return (
        <div style={{
            backgroundColor: '#e0efff',
          }}>
            <br/>
            <Container><h1 style={{color: '#007bff'}}>Venta</h1></Container>
            <br/>
            <Container>
            <Form.Control placeholder="¿Que desea buscar?" onChange={busqueda}/>
            </Container>
            <br/>
            <Container >
            <Table striped bordered hover >
                <thead>
                    <tr>
                    <th>Codigo</th>
                    <th>Producto</th>
                    <th>Cantidad bodega</th>
                    <th>Precio Uni. </th>
                    <th>Precio Uni. s/IVA </th>
                    <th>Cantidad</th>
                    <th>Agregar</th>
                    </tr>
                </thead>
                <tbody>
                    {productsData.map((product, index) => 
                        <tr key={product.codigoBarra}>
                            <td>{product.codigoBarra}</td>
                            <td>{product.name}</td>
                            <td >{product.cantidad}</td>
                            <td>${product.precio}</td>
                            <td>${product.precioIVA}</td>
                            <td ><FormControl className="center" onChange={handleCantChange} style={{width: 50}} /></td>
                            <td><RiAddCircleLine onClick={() => addProductBoleta({
                                                                                    ...product,
                                                                                    'cantidadV':parseInt(cant,10)
                                                                                    })} style={{height: 30, width: 30,color: 'green'}}/></td>
                        </tr>
                     )}
                </tbody>
            </Table>
            <h1>Detalle de venta</h1>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>Codigo</th>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Precio Uni.</th>
                    <th>Precio Uni. s/IVA</th>
                    <th>Precio Tot.</th>
                    <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    {boletaData.map((product, index) => 
                        <tr key={index}>
                            <td>{product.codigoBarra}</td>
                            <td>{ product.name}</td>
                            <td>{ product.cantidadV}</td>
                            <td>${product.precio}</td>
                            <td>${product.precioIVA}</td>
                            <td>${product.cantidadV * product.precio}</td>
                            <td><RiCloseCircleLine onClick={() => deleteProductBoleta(index)} style={{height: 30, width: 30,color: 'red'}}/></td>
                        </tr>
                     )}
                </tbody>
            </Table>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>TOTAL s/IVA:</th>
                    <th>${total/1.19}</th> 
                    </tr>
                </thead>
            </Table>   
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th><h3>TOTAL:</h3></th>
                    <th><h3>${total}</h3></th> 
                    </tr>
                </thead>
            </Table>        

            
            <div className="justify-content-center h-100"><Button onClick={() => agregarBoleta()} variant="success" size="lg" disabled={btnState}  >Cancele en caja</Button></div>
            <br/>
            <br/>
            

            </Container>
        </div>
    )
}

export default ProductsList
