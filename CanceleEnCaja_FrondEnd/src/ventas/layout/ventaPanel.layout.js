import SpinnerLoader from '../components/spinner.component'
import { useState } from 'react'
import ProductsList from '../components/productsList.component'
import React from "react";



function VentaPanel (props) {
    const [loaded, setLoadedState] = useState(false)
    
    return (
        <div >
            <SpinnerLoader dataLoaded={loaded}/>
            <ProductsList setLoadedState={setLoadedState} loaded={loaded}/>
        </div>
    )
}

export default VentaPanel