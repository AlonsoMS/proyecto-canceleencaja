import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import VentaPanel from './ventas/layout/ventaPanel.layout'
import BoletaPanel from './caja/layout/cajaPanel.layout'
import InventarioPanel from './inventario/layout/inventarioPanel.layout'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Button, Col,Row  } from 'react-bootstrap';
import Image from './CanceleEnCaja.png';

function App() {

  return (    
    <>
    <div className="justify-content-center h-100" style={{
            backgroundColor: 'black',
          }}>
     <div className="row justify-content-center h-100"><img style={{height: 200, width: 450}} src={Image} /></div>
    <Router>
    <div className="row justify-content-center h-100">
    
      <Row>
        <Col>
        <Button href="/venta" variant="outline-primary">VENTA</Button></Col>
        
        <Col>
        <Button href="/inventario" variant="outline-success">INVENTARIO</Button></Col>
        
        <Col>
        <Button href="/caja" variant="outline-danger">CAJA</Button></Col>
        </Row>
        <br/>
        <br/>
        <br/>
      </div>
      <Switch>
        <Route exact path="/venta" component={VentaPanel}/>
        <Route exact path="/inventario" component={InventarioPanel}/>
        <Route exact path="/caja" component={BoletaPanel}/>

      </Switch>
    </Router>
    </div>
  </>
  );
}

export default App;
