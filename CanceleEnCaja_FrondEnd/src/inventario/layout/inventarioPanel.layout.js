import SpinnerLoader from '../components/spinner.component'
import { useState } from 'react'
import InventarioList from '../components/inventarioList.component'


function InfoPanel (props) {
    const [loaded, setLoadedState] = useState(false)
    
    return (
        <div style={{
            backgroundColor: '#b2eec0',
          }}>
            <SpinnerLoader dataLoaded={loaded}/>
            <InventarioList setLoadedState={setLoadedState} loaded={loaded}/>
        </div>
    )
}

export default InfoPanel