import { useState, useEffect} from 'react'
import axios from 'axios'
import { Container, Form ,Table, FormControl } from 'react-bootstrap'
import { RiEditCircleFill, RiRefreshLine, RiSave3Fill, RiCloseCircleLine } from "react-icons/ri";

function ProductsList (props) {
    const [productsData, setProductsData] = useState([])
    const [boletaData, setBoletaData] = useState([])
    const [productsDataTmp, setProductsDataTmp] = useState([])
 
    const [Name, setName] = useState('')
    const [Cantidad, setCantidad] = useState(0)
    const [Precio, setPrecio] = useState(0)

   
    const [CodigoBarraNew, setCodigoBarraNew] = useState(0)
    const [NameNew, setNameNew] = useState('')
    const [CantidadNew, setCantidadNew] = useState(0)
    const [PrecioNew, setPrecioNew] = useState(0)

  
    const handleNameChange = (event) => {
        setName(event.target.value)
    }
    const handleCantidadChange = (event) => {
        setCantidad(parseInt(event.target.value,10))
    }
    const handlePrecioChange = (event) => {
        setPrecio(parseInt(event.target.value,10))
    }

    const handleCodigoBarraNewChange = (event) => {
        setCodigoBarraNew(parseInt(event.target.value,10))
    }
    const handleNameNewChange = (event) => {
        setNameNew(event.target.value)
    }
    const handleCantidadNewChange = (event) => {
        setCantidadNew(parseInt(event.target.value,10))
    }
    const handlePrecioNewChange = (event) => {
        setPrecioNew(parseInt(event.target.value,10))
    }

    const busqueda = (event) => {
        let tmp3=[]
        for (let i=0;i<productsDataTmp.length;i++){
            let codigoTmp=(productsDataTmp[i].codigoBarra).toString()
            let nameTmp=productsDataTmp[i].name
            console.log(nameTmp)
            if (nameTmp.includes(event.target.value.toUpperCase()) ||  codigoTmp.includes(event.target.value)){
                tmp3.push(productsDataTmp[i])
            }
        }
        setProductsData(tmp3)
    }



    


    const eliminarProducto = async(producto) =>{

        const eliminar = await axios.delete('http://localhost:3001/inventario/eliminarProducto/' + producto.codigoBarra)   
        console.log(eliminar.data)
        if(eliminar.data.status ==='OK'){
            let tmp = productsData
            let index = tmp.indexOf(producto)
            console.log(tmp[index].codigoBarra)
            //deleteProductInventario(index)
        }
        
    }

    const actualizarLosProductos = (producto)=>{
        if(Name===''){
            setName(producto.name)
        }
        else{
            producto.name=Name
        }
        if(Cantidad===0){
            setCantidad(parseInt(producto.cantidad,10))
        }
        else{
            producto.cantidad=Cantidad
        }
        if(Precio===0){
            setPrecio(parseInt(producto.precio,10))
        }
        else{
            producto.precio=Precio
        }
        actualizarProducto(producto)
    }

    const actualizarProducto = async(producto) =>{
        
        const actualizar = await axios.post(('http://localhost:3001/inventario/agregarProducto'),     
                        {
                            "codigoBarra": parseInt(producto.codigoBarra,10),
                            "name": producto.name,
                            "cantidad": parseInt(producto.cantidad,10),
                            "precio": parseInt(producto.precio,10)
                            }    
                  )
                  if(actualizar.data.status === 'OK'){
                    /*let tmp = productsData
                    tmp.push({
                        "codigoBarra": parseInt(producto.codigoBarra,10),
                        "name": producto.name,
                        "cantidad": parseInt(producto.cantidad,10),
                        "precio": parseInt(producto.precio,10)
                        })
                    setProductsData(tmp)*/
                  }
                  console.log(actualizar.data) 
    }



    const agregarProducto = async() =>{
        const agregar = await axios.post(('http://localhost:3001/inventario/agregarProducto'),     
                        {
                            "codigoBarra": CodigoBarraNew,
                            "name": NameNew,
                            "cantidad": CantidadNew,
                            "precio": PrecioNew
                            }    
                  )
                  if(agregar.data.status === 'OK'){
                    let tmp = productsData
                    tmp.push({
                        "codigoBarra": CodigoBarraNew,
                        "name": NameNew,
                        "cantidad": CantidadNew,
                        "precio": PrecioNew
                        })
                    setProductsData(tmp)
                  }
                  console.log(agregar.data) 
    }
    


    useEffect(() => {
        const fetchData = async () => {
            if (!props.loaded) {
                const result = await axios.get('http://localhost:3001/inventario')
                console.log(result.data)
                if (result.data.status === "OK") { 
                    props.setLoadedState(true)
                    setProductsDataTmp(result.data.productos)
                    setProductsData(result.data.productos)
                }
                
            }


        }
        fetchData()
    })
    return (
        <div style={{
            backgroundColor: '#cdffd8',
          }}>
            <br/>
            <Container>
            <h1 style={{color: '#28a745'}}>Inventario</h1>
            </Container>
            <br/>
            <Container>
            <Form.Control placeholder="¿Que desea buscar?" onChange={busqueda}/>
            </Container>
            <br/>
            <Container>
            
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>Codigo</th>
                    <th>Producto</th>
                    <th>Cantidad bodega</th>
                    <th>Precio Uni</th>
                    <th>Precio Uni. s/IVA</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    {productsData.map((product, index) => 
                        <tr key={index}>
                            <td>{product.codigoBarra}</td>
                            <td>{product.name}</td>
                            <td>{product.cantidad}</td>
                            <td>{product.precio}</td>
                            <td>{product.precioIVA}</td>
                            <td><RiEditCircleFill onClick={() => setBoletaData([product])} style={{height: 30, width: 30,color: 'green'}}/></td>
                            <td><RiCloseCircleLine onClick={() => eliminarProducto(product)} style={{height: 30, width: 30,color: 'red'}}/></td>
                        </tr>
                     )}
                </tbody>
            </Table>
            <br/>
            <Container/>
            <Container>
            <h1 style={{color: '#28a745'}}>Editar</h1>
            </Container>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>Codigo</th>
                    <th>Producto</th>
                    <th>Cantidad en bodega</th>
                    <th>Precio Uni.</th>
                    <th>Actualizar</th>
                    </tr>
                </thead>
                <tbody>
                    {boletaData.map((product, index) => 
                        <tr key={index}>
                            <td>{product.codigoBarra}</td>
                            <td><FormControl className="col-xs-1" Value={product.name} onChange={handleNameChange}/></td>
                            <td><FormControl className="col-xs-1" Value={product.cantidad} onChange={handleCantidadChange}/></td>
                            <td><FormControl className="col-xs-1" Value={product.precio} onChange={handlePrecioChange}/></td>
                            <td><RiRefreshLine onClick={() => actualizarLosProductos(product)} style={{height: 30, width: 30,color: 'blue'}}/></td>
                        </tr>
                     )}
                </tbody>

            </Table>
            <br/>
            <Container>
            <h1 style={{color: '#28a745'}}>Agregar producto a inventario</h1>  
            </Container>      
            <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>Codigo</th>
                        <th>Producto</th>
                        <th>Cantidad en bodega</th>
                        <th>Precio Uni.</th>
                        <th>Agregar</th>
                    </tr>
                    </thead>
                    <tbody>
        
                        <tr>
                            <td><FormControl className="col-xs-1" onChange={handleCodigoBarraNewChange}/></td>
                            <td><FormControl className="col-xs-1" onChange={handleNameNewChange}/></td>
                            <td><FormControl className="col-xs-1" onChange={handleCantidadNewChange}/></td>
                            <td><FormControl className="col-xs-1" onChange={handlePrecioNewChange}/></td>
                            <td><RiSave3Fill onClick={() => agregarProducto()} style={{height: 30, width: 30,color: 'green'}}/></td>
                        </tr>
                     
                    </tbody>
                        
            </Table> 
            <br/>        
            </Container>
        </div>
    )
}

export default ProductsList
