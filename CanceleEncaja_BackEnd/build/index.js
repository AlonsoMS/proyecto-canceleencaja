"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var body_parser_1 = __importDefault(require("body-parser"));
var typeorm_1 = require("typeorm");
var express_1 = __importDefault(require("express"));
var inventarioController = __importStar(require("./Controller/inventarioController"));
var boletaController = __importStar(require("./Controller/boletaController"));
var server = express_1["default"]();
server.use(body_parser_1["default"].json());
server.use(body_parser_1["default"].urlencoded({
    extended: true
}));
typeorm_1.createConnection().then(function () { return console.log('Conexión creada'); })["catch"](function (error) { return console.log('Error en la conexión', error); });
server.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
server.get('/defaultMessage', inventarioController.sendDeafultMessage);
server.post('/inventario/agregarProducto', inventarioController.agregarProductoInventario);
server.post('/venta/agregarBoleta', boletaController.agregarBoleta);
server.get('/verBoletas', boletaController.showBoletas);
server["delete"]('/inventario/eliminarProducto/:codigoBarra', inventarioController.deleteProducto);
server.get('/inventario', inventarioController.showProductos);
server.get('/inventario/buscarProducto/name/:name', inventarioController.buscarNameProducto);
module.exports = server;
server.listen(3001, function () {
    console.log('Server listening at port 3001');
});
//# sourceMappingURL=index.js.map