import bodyParser from 'body-parser'
import { createConnection } from 'typeorm'
import express, { Express } from 'express'

import * as inventarioController from './Controller/inventarioController'
import * as boletaController from './Controller/boletaController'

const server: Express = express()

server.use(bodyParser.json())
server.use(bodyParser.urlencoded({
    extended: true
}))
createConnection().then(
  () => console.log('Conexión creada')
).catch(
  error => console.log('Error en la conexión', error)
)

server.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});


//app.use(express.static(__dirname + '/public/'));
server.get('/defaultMessage', inventarioController.sendDeafultMessage)
server.post('/inventario/agregarProducto',inventarioController.agregarProductoInventario)
server.post('/venta/agregarBoleta',boletaController.agregarBoleta)
server.get('/verBoletas',boletaController.showBoletas)
server.delete('/inventario/eliminarProducto/:codigoBarra', inventarioController.deleteProducto)
server.get('/inventario', inventarioController.showProductos)
server.get('/inventario/buscarProducto/name/:name',inventarioController.buscarNameProducto ) //Tenemos que buscar ej: .../ampolletas y que salgan todos los que tengan el nombre ampolletas

module.exports = server

server.listen(3001, () => {
  console.log('Server listening at port 3001')
})