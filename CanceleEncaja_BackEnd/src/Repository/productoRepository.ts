import "reflect-metadata"
import { Producto } from '../Model/producto'
import productoModel from './productos.model.interface'
import { getRepository } from "typeorm"

let Response
export const agregarProductoRepository = async (productoRegister:productoModel) => {
    try{

        const producto = new Producto()
        producto.codigoBarra = productoRegister.codigoBarra
        producto.name = productoRegister.name.toUpperCase()
        producto.cantidad = productoRegister.cantidad
        producto.precio = productoRegister.precio
        producto.precioIVA = productoRegister.precio/1.19
        
        //if(await getRepository(User).findOne({email:user.email})){eval("Error voluntario")}  Para verificar si es que el producto ya existe
        await getRepository(Producto).save(producto)
        const productos =await getRepository(Producto).find()
        
        Response={
                status:"OK",
                message:"Producto agregado correctamente",
                productos:productos
                }  

    }catch{   
        Response={
                status:"NOK",
                message:"Error al agregar producto"
                }
        }
    return Response
}

export const deleteProductoRepository = async (codigoBarra:number) => {
    try{
        const producto:Producto =await getRepository(Producto).findOne(codigoBarra)
        if(producto){
            await getRepository(Producto).remove(producto)
            const productos =await getRepository(Producto).find()
            Response={
                    status:"OK",
                    message:"Producto deleted correctly",
                    productos:productos
                    }
        }
        else{
            Response={
                    status:"NOK",
                    message:"Producto not found"
                    }
        }
        
    }catch(error){
        console.log(error)
        Response={
                status:"NOK",
                message:"There was an error deleting producto"
                } 

    }
    return Response

}
export const showProductosRepository = async () => {
    try{
        const productos =await getRepository(Producto).find()
        Response={
                status:"OK",
                message:"Productos displayed correctly",
                productos:productos
                }
    }catch{
        Response={
                status:"NOK",
                message:"Error displaying productos"
                }

    }
    return Response
    
}

export const buscarNameProductoRepository = async (nombre: string) => {
    try{
        const producto =await getRepository(Producto).find({name:nombre.toUpperCase()})
        Response={
                status:"OK",
                message:"Productos displayed correctly",
                producto:producto
                }
    }catch{
        Response={
                status:"NOK",
                message:"Error displaying productos"
                }

    }
    return Response
    
}