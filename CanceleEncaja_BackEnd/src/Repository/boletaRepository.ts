import "reflect-metadata"
import { Boletas } from '../Model/boletas'
import boletasModel from './boletas.model.interface'
import { getRepository } from "typeorm"

let Response
export const agregarBoletaRepository = async (boletaRegister:boletasModel) => {
    try{

        const boleta = new Boletas()
        boleta.fecha_hora=boletaRegister.fecha_hora
        boleta.monto=boletaRegister.monto
        
        //if(await getRepository(User).findOne({email:user.email})){eval("Error voluntario")}  Para verificar si es que el producto ya existe
        await getRepository(Boletas).save(boleta)
        const boletas =await getRepository(Boletas).find()
        Response={
                status:"OK",
                message:"Boleta agregada correctamente",
                boletas:boletas
                }  

    }catch{   
        Response={
                status:"NOK",
                message:"Error al agregar boleta"
                }
        }
    return Response
}

export const showBoletaRepository = async () => {
    try{
        const boletas =await getRepository(Boletas).find()
        Response={
                status:"OK",
                message:"Productos displayed correctly",
                boletas:boletas
                }
    }catch{
        Response={
                status:"NOK",
                message:"Error displaying productos"
                }

    }
    return Response
    
}