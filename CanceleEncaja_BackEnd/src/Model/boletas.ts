import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'
import productoModel from '../Repository/productos.model.interface'

@Entity()
export class Boletas {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fecha_hora: string

    @Column()
    monto: number
}