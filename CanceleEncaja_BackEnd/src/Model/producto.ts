import { Entity, Column, PrimaryColumn } from 'typeorm'

@Entity()
export class Producto {
    @PrimaryColumn()
    codigoBarra: number

    @Column()
    name: string
    
    @Column()
    cantidad: number
   
    @Column()
    precio:number
    
    @Column({ type: 'decimal' })
    precioIVA:number
}
