import { Request, Response } from 'express'
import * as productoRepository from '../Repository/productoRepository'

export const sendDeafultMessage = (request: Request, response: Response) => {
    response.json({message:"ok"})
}

export const agregarProductoInventario = async (request: Request, response: Response) => {
    const res = await productoRepository.agregarProductoRepository(request.body)
    response.json(res)
}

export const deleteProducto = async (request: Request, response: Response) => {
    const res = await productoRepository.deleteProductoRepository(parseInt(request.params.codigoBarra, 10))
    response.json(res)
}

export const showProductos = async (request: Request, response: Response) => {
    const res = await productoRepository.showProductosRepository()
    response.json(res)
}

export const buscarNameProducto = async (request: Request, response: Response) => {
    const res = await productoRepository.buscarNameProductoRepository(request.params.name)
    response.json(res)
}