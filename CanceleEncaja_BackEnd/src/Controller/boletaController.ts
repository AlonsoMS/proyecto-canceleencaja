import { Request, Response } from 'express'
import * as boletaRepository from '../Repository/boletaRepository'

export const agregarBoleta = async (request: Request, response: Response) => {
    const res = await boletaRepository.agregarBoletaRepository(request.body)
    response.json(res)
}
export const showBoletas = async (request: Request, response: Response) => {
    const res = await boletaRepository.showBoletaRepository()
    response.json(res)
}